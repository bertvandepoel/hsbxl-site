---
title: "How to get to HSBXL?"
linktitle: "How to get to HSBXL?"
---

**Address:** Rue de la Petite Île 1, 1070 Anderlecht, Brussels, Belgium.
**Build name:** Studio CityGate

# How to get to the address

{{< image src="/images/enter_space/001.png">}}

## By public transport 🚆

You can get to Brussels South Train Station (Bruxelles-Midi, Brussel-Zuid) using trains, the metro, the tram or busses. 

From Brussels South Train Station:  
The space is located at 1200m from Brussels south transtation, about a 15 minute walk [Route on OpenStreetMap](https://www.openstreetmap.org/directions?engine=fossgis_valhalla_foot&route=50.83613%2C4.33441%3B50.83219%2C4.32177)

From Brussels South Train Station you can also take a bus that stops near the hackerspace:
* [STIB bus 78](https://www.stib-mivb.be/horaires-dienstregeling2.html?l=en&_line=78&_directioncode=V&_mode=rt) ( stop "Deux Gares" ) - 200m walk
* [STIB bus 73](https://www.stib-mivb.be/horaires-dienstregeling2.html?l=en&_line=73&_directioncode=V&_mode=rt) ( stop "Deux Gares" ) - 200m walk

Coming from ULB Solbosch (e.g. you're at FOSDEM and you want to go to Bytenight)? Walk to tram stop "Cambres-etoile" and take [STIB tram 7](https://www.stib-mivb.be/horaires-dienstregeling2.html?l=en&_line=7&_directioncode=V&_mode=rt) towards Churchill. This tram will become tram 3 towards Esplanade when you arrive at Churchill. You can get off at Gare du Midi and take bus 73 or 78 as outlined above.

## By car 🚘
Most of the time, there are free parking spots in Rue des Goujons. At the front gate (Rue de la petite île), there's toll parking (since the summer of 2022).

## Shared mobility
Brussels has a [vast offering in shared mobility](https://www.brussels.be/alternative-mobility). Use a bike, step or even car using the appropriate plans.

Keep in mind that Brussels has enforced drop zones for shared mobility bikes, electric scooters and kick scooters (aka trottinette or step). Please park responsibly!
## At night: Collecto

To get back at your home/hotel, there is [Collecto](https://en.collecto.be/). A Taxi picks you up at certain points (many STIB public transport stops) and brings you to your destination within the Brussels region for the fixed price of 6 euro. 

You can order a Collecto at any time between 11 p.m. and 6 a.m, but it is advisable to order at least 20 minutes in advance. If you do not have a smartphone to use the application ([Android](https://play.google.com/store/apps/details?id=be.tradecom.collecto&hl=en&gl=US) - [Apple](You can order a Collecto at any time between 11 p.m. and 6 a.m, but it is advisable to order at least 20 minutes in advance. If you do not have a smartphone to use the application, you can also call 02/800 36 36 to reserve your Collecto.)), you can also call 02/800 36 36 to reserve your Collecto.

There is a [collecto](https://taxisverts.be/en/collecto-en/) startpoint around the corner at the "Deux Gares" STIB bus stop, at 200m. see https://www.openstreetmap.org/directions?engine=fossgis_valhalla_foot&route=50.83196%2C4.32460%3B50.83219%2C4.32177

# How to get to the Hackerspace once you have reached the address
Gates and doors are often closed and locked, since the Studio CityGate building offers space to many different organisations and artists but can also attract undesired visitors. If you get stuck then please call [+32 2 880 40 04](tel:003228804004) to reach the hackerspace and someone will come down to help you. When you become a member of HSBXL you will receive the necessary codes to enter on your own. 

## Enter the Pink Gate and walk to Door A

Two different gates grant access to the parking lot of Studio CityGate, one in Rue des Doujons and one in Rue de la Petite Ile. The latter is painted pink and is the easiest to access and find. The exact address of the Address of the pink gate is _Rue de la Petite Ile 1_ (_Klein Eilandstraat 1_)

[OpenStreetMap Pin Point](https://www.openstreetmap.org/?mlat=50.83175&mlon=4.32254#map=18/50.83175/4.32254)

{{< image src="/images/enter_space/002.png">}}

{{< image src="/images/enter_space/003.jpg">}}

## Find the door with the A
Once you are in the parking lot, look for the door with the A. This door has a keycode and is always locked, so if you are not a member it's best to call now ([+32 2 880 40 04](tel:003228804004)) so someone can come down and open up the door. If you can't find the door with the A, they can also help find you.

{{< image src="/images/enter_space/004.jpg">}}

## Up the stairs and follow the arrows
Once you are inside, you go up several flights of stairs and follow the arrows and HSBXL-related signage until you arrive at the hacker space. You can always call if you get lost.
### First stairs
{{< image src="/images/enter_space/005.jpg">}}

### Second stairs
{{< image src="/images/enter_space/006.jpg">}}

### Third stairs
{{< image src="/images/enter_space/007.jpg">}}

### Follow the black arrows
{{< image src="/images/enter_space/008.jpg">}}
{{< image src="/images/enter_space/009.jpg">}}

### Finally there
{{< image src="/images/enter_space/010.jpg">}}

