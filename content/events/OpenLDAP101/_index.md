---
linktitle: "OpenLDAP 101"
title: "OpenLDAP 101"
location: "HSBXL"
eventtype: "workshop"
series: "BreakOpen"
start: "true"
startdate:  2023-04-25
starttime: "20:00"
endtime: "21:00"
---

Want to have your own user directory at home or want to make your awesome project accesible by all HSBXL members?

Everything you need to know to get started with OpenLDAP.
We'll be using the spaces OpenLDAP directory as a starting point.