---
linktitle: "Hackerspace Blueprint"
title: "Hackerspace Blueprint"
location: "HSBXL"
eventtype: "workshop"
price: "membership"
aliases: [/blueprint/]
series: "Blueprint"
start: "true"
---

During a few weeks, we'll try to think about how to practically run a hackerspace - what are some ground rules to work around and have a sustainable space? We'll base upon the work done by [Hackerspace Ghent](https://github.com/0x20/hackerspace-blueprint) (originally HTH, now "hackerspace blueprint") and have [our own fork](https://gitlab.com/hsbxl/hackerspace-blueprint).

What are our current (unwritten) rules? Now is the time to challenge them.

## Upcoming Blueprint brainstorms
{{< events when="upcoming" series="Blueprint" >}}

## Past Blueprint brainstorms
{{< events when="past" series="Blueprint" >}}