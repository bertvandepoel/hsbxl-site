---
startdate:  2020-01-11
starttime: "14:30"
endtime: "17:00"
linktitle: "Tech with kids"
title: "Tech with kids (NL-FR-EN-...)"
price: "Free"
image: "tech_with_kids"
series: "Tech With Kids"
eventtype: "for kids age 6+ with parents"
location: "HSBXL"
---

Tech With Kids is a get together of kids with parents to work on projects, learn new things, play with tech.
We can use the 3D-printer (notice, this is slow!), the laser cutter, the electro lab, the workbench tools, the internet connection... but most of all, each other's creativity and knowledge!

There are no prepared projects, but you're always free to be inspired locally or think for an idea!

It's an informal gathering, we speak the language of the kids (Dutch, French, English, ...).