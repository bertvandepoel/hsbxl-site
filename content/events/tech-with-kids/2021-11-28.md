---
startdate:  2021-11-28
starttime: "14:30"
endtime: "17:00"
linktitle: "Tech with kids - Let's grow chrystals"
title: "Tech with kids - Let's grow chrystals (NL-FR-EN-...)"
price: "Free"
image: "tech_with_kids"
series: "Tech With Kids"
eventtype: "for kids age 6+ with parents"
location: "HSBXL"
---

This workshop will be given in Dutch, but we're fluent in English and French too.
Description below is in Dutch.

## Kristallen kweken op verschillende manieren
We bekijken een aantal klaargemaakte preparaten onder de microscoop
We prepareren een oplossing waarin kristallen kunnen groeien.

De kristallen moeten 2 weken rusten
- ofwel neem je je preparaten mee naar huis en rusten ze daar
- ofwel laat je de preparaten rusten in de hackerspace en kom je ze ophalen op 12 december (volgende tech with kids)

## Wat moet je zelf meebrengen:
- 2 glazen bokalen (propere confituurpotten, glas - voor veilig transport best met een deksel) 
- 1 plastic bakje (200 gram préparé bij de slager, of tupperware of zo)
- schoendoos of curverdoos of zo als je de preparaten veilig terug mee wil nemen naar huis

## De hackerspace
De resterende tijd kunnen kinderen met hun ouders de hackerspace verkennen, of de apparatuur gebruiken om te werken aan hun eigen project. Kinderen werken steeds onder toezicht van hun ouder(s).

## Inschrijven
Inschrijven is nodig om het aantal plaatsen te bewaken en voldoende grondstoffen te kunnen voorzien.
Inschrijven doe je door in de commentaar je naam te plaatsen (het e-mail adres is niet zichtbaar maar moet ingevuld worden) en in te vullen met hoevelen jullie komen. Je krijgt bevestiging van je inschrijving per mail.
