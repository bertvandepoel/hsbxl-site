---
startdate:  2022-03-20
starttime: "14:30"
endtime: "17:00"
linktitle: "Tech with kids - Virtual Reality"
title: "Tech with kids - Virtual Reality (NL-FR-EN-...)"
price: "Free"
image: "vr-workshop"
series: "Tech With Kids"
eventtype: "for kids age 6+ with parents"
location: "HSBXL"
---

## Language
This workshop will be given in Dutch, but French and English are not an issue if someone has difficulty following.

## Workshop
During this workshop, we'll discover the basic ideas behind virtual reality and try out a few things using a Google Cardboard.
Participants go home with Google Cardboard VR goggles to be used with your smartphone.

## Requirements
- Bring along your favorite drawing pencil and maybe a few colours.
- You'll also need an Android (4.4+) or iOS (8.0+) smartphone (a tablet is too big).

## Discovering the space
The second half of the afternoon, kids with their parent(s) will be able to discover the space together or can use the tools to work on their own project. Children can only use the tools under supervision of their parent(s).

## Registration
Registration to the workshop is required as we only have a limited number of headsets.
Put your name in the comments and tell us with how many you'll come. Add your e-mail in the appropriate field - this won't be visible on the site but allows to confirm your presence.



Photo by Jessica Lewis Creative from Pexels
