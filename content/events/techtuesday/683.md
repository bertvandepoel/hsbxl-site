---
techtuenr: "683"
startdate: 2023-05-30
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue683
series: TechTuesday
title: TechTuesday 683
linktitle: "TechTue 683"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
