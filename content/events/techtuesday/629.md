---
techtuenr: "629"
startdate: 2022-05-17
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue629
series: TechTuesday
title: TechTuesday 629
linktitle: "TechTue 629"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
