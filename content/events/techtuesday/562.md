---
eventid: techtue560
startdate:  2020-02-25
starttime: "19:00"
linktitle: "TechTue 560"
title: "TechTuesday 560"
price: ""
image: "techtuesday.jpg"
series: "TechTuesday"
eventtype: "Social get-together"
location: "HSBXL"
---

Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
