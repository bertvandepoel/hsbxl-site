---
techtuenr: "689"
startdate: 2023-07-11
starttime: 19:00
price: ""
eventtype: Social get-together
eventid: techtue689
series: TechTuesday
title: TechTuesday 689
linktitle: "TechTue 689"
location: HSBXL
image: techtuesday.png
---
Techtuesdays are a social meet-up sort of thing, just walk in for a talk, club-mate, latest news, or to show off your latest pet-project ... or you can always stay home and read slashdot.
