---
startdate:  2023-02-03
starttime: "13:00"
enddate:  2023-02-03
endtime: "19:00"
allday: true
linktitle: "Matrix Community Meetup"
title: "Matrix Community Meetup"
location: "HSBXL"
eventtype: "Barcamp"
price: "Free"
series: "byteweek2023"
image: "matrix-favicon-white.png"
--- 

# Matrix Community Meetup
There is a large interest in FOSDEM from the Matrix community, and some folks have decided the officially alotted time [for the Matrix track](https://fosdem.org/2023/schedule/track/matrix/) at FOSDEM is not enough and are organizing this additional pre-FOSDEM meetup.
Find us at [#fosdem23-community-meetup:matrix.org](https://matrix.to/#/#fosdem23-community-meetup:matrix.org) for more info and to enter your suggestions what might happen.

Hackerspace Brussels has graciously offered to supply the location for this event.
There's heating, food, WiFi, and a setup to give presentations, do workshops, and more.
Don't miss the [HSBXL beer tasting event](../hsbxl-beer-event/) in the evening!
