---
startdate: 2024-01-31
starttime: "09:00"
enddate: 2024-01-31
endtime: "18:00"
allday: true
linktitle: "FreeCAD hackathon - Day 1"
title: "FreeCAD hackathon - Day 1"
location: "HSBXL"
eventtype: "Hackaton"
price: "Free"
series: "byteweek2024"
image: "FreeCAD.svg"
---

# FreeCAD Hackathon - Day 1

The [FreeCAD project](https://freecad.org) is organizing this year again a hackathon at HSBXL, prior to the [FreeCAD day](https://blog.freecad.org/2023/10/31/fosdem-freecad-day-and-hackathon-2024/) and FOSDEM. We will sit around to code and try to solve some issues together. If you'd like to dive into the FreeCAD source code, that's your opportunity! Come and join us! There will also be a presence on [Discord](https://discord.com/invite/uh85ZRNcfk). More details on the [FreeCAD blog](https://blog.freecad.org/2023/10/31/fosdem-freecad-day-and-hackathon-2024/).
