---
title: "Our Own Cloud-out-of-the-cloud storage"
linktitle: "Nextcloud @HSBXL"
date: 2022-09-10
state: running
maintainer: "jurgen"
---

# Context and background
Let's set up a server running [Nextcloud](https://nextcloud.com/) and [Collabora Office](https://www.collaboraoffice.com/collabora-online-and-nextcloud/) inside the space. This will allow for:
- easily sharing documents
- creating project documentation that can then be shared publicly
- create spin-off applications that interact with a nextcloud ecosystem. Think of a ~~sports tracker~~ (who are we kidding?) *shopping list app* that stores its data in a nextcloud folder instead of a centralized service)
- see if we can do something with the [SOLID](https://solidproject.org/) concept that the Flemish government is working on (decentralized cryptographically safe storage of your personal information). There exists a [rudimentary POC for SOLID inside Nextcloud](https://apps.nextcloud.com/apps/solid). Can we set up our own pods and maybe even learn some things along the way?

Jurgen has set up Nextcloud on a local computer in the past - it's not that hard.

You can find documentation in our cloud (let's dogfood this):
- There's an [internal link](https://cloud.hsbxl.be/index.php/f/3011) for editors
- There's a [public link](https://cloud.hsbxl.be/index.php/s/xqzsoGCRWmHLA4r?path=%2FSetting%20up%20the%20Nextcloud%20server) if you want to follow along.

As soon as this project reaches a (more or less) static state, it can easily be converted into webpages (as the content is plain markdown).